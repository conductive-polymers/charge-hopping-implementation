#ifndef CHARGE_H
#define CHARGE_H

#include <random>
#include <chrono>
#include <iostream>
#include <ctime>
#include <math.h>

class Charge{

	private:
		//Variables var;

	public:
		int N;
		int bulk_size;

		double dt	= var.dt;
		double k_inter	= var.k_inter;
		double k_intra	= var.k_inter;

		double sum_of_rates = 0;

		double next_step= 0;

		double prob;

		int current_monomer;
		Vector3d position;

		unsigned int n_bound = 0;
		unsigned int n_unbound = 0;

		Charge(int bulkSize, int polymer_length){
			bulk_size = bulkSize;
			N = polymer_length;
			current_monomer = 0;
			position = Vector3d(0,0,0);
		}



		void Integration_step(double random_uniform, double hop_dir, std::vector<Vector3d>* vec_distances, std::vector<long double>* abs_distances, Vector3d monomer_shift, double t_intrinsic){

			updatePosition(monomer_shift);
			if(next_step <= t_intrinsic){
				hopping_step(hop_dir, vec_distances, abs_distances);
				calculate_next_step(t_intrinsic, random_uniform);
			}
		}

		void calculate_next_step(double intrinsic_time, double random_uniform){
			next_step = intrinsic_time + 1/sum_of_rates*log(1/random_uniform);
		}

		bool hop_allowed(double distance){
			
			return distance<=var.rc;

		}


		double k_inter_rate(double distance){

			double k = var.k_inter*exp(-var.alpha*distance);
			return k;

		}

		bool chargeAtBeginning(){
			if(current_monomer-floor(current_monomer/N)*N==0){
				return true;
			} else {
				return false;
			}
		}

		bool chargeAtEnd(){
			if(current_monomer+1-floor((current_monomer+1)/N)*N==0){
				return true;
			} else {
				return false;
			}
		}

		void hopping_step(double hop_dir, std::vector<Vector3d>* vec_distances, std::vector<long double>* abs_distances){
			
			/*
		
				kMC framework for charge hopping

			*/

			std::vector<int> possible_targets;			// Vector to keep track of all monomers within a reactive distance rc
			std::vector<long double> hop_probablity;		// Vector to keep track of probabilities to hop to a monomer defined in possible_targets
			std::vector<long double> cumulative_prob{0};		// Cumulative probability

			for(int m=0; m<abs_distances->size();m++){


				// Check if monomer m is within reactive distance of current charge position
				if( (*abs_distances)[m]<=var.rc ){

					possible_targets.push_back(m);
					if( chargeAtEnd()  && (m==current_monomer-1) ){
						// Probability for interchain hop if charge is on the end of a chain
						hop_probablity.push_back(k_inter_rate((*abs_distances)[m]));

					} else if( chargeAtBeginning() && (m==current_monomer+1) ){
						// Probability for interchain hop if charge is on the end of a chain
						hop_probablity.push_back(k_inter_rate((*abs_distances)[m]));

					} else if(m==current_monomer+1 || m==current_monomer-1){
						// Probability charge hops to bound neighbor
						hop_probablity.push_back(var.k_intra);
				
	
					} else {
						// Probability for interchain hop (includes hops to mirror images of polymer)
						hop_probablity.push_back(k_inter_rate((*abs_distances)[m]));
						

					}
						
					cumulative_prob.push_back(cumulative_prob[cumulative_prob.size()-1]+hop_probablity[hop_probablity.size()-1]);
				}
			}
			
			sum_of_rates = cumulative_prob[cumulative_prob.size()-1];
			std::cout << cumulative_prob[cumulative_prob.size()-1];	

			// Normalization of cumulative probability
			for(int i=1; i<cumulative_prob.size(); i++){

				cumulative_prob[i]=cumulative_prob[i]/cumulative_prob[cumulative_prob.size()-1];

				// Hop is performed according to kMC framework
				if(hop_dir<=cumulative_prob[i]){

					if (current_monomer+1 == possible_targets[i-1] || current_monomer-1 == possible_targets[i-1]){
						n_bound+=1;				
					} else {
						n_unbound+=1;
					}
					current_monomer = possible_targets[i-1];
					break;
				}
			}	
			
			// Updates position of charge by hopping distance wrapped around PBC
			updatePosition((*vec_distances)[current_monomer]);

		}

		void updatePosition(Vector3d distance){
			position+=distance;
		}
		
};

#endif
