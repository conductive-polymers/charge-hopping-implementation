#ifndef VARIABLES_H
#define VARIABLES_H

struct Variables{
	// Ovito
	bool writeOvito = true;


	// Potential
	double C_l	=	30;		// Harmonic bonds strength

	double sigma 	= 	1.0;		
	double rc	= 	2.5;		// Reactive distance (Distance charge can hop)
	double eps	=	0.1;		// Solvent quality

	// Simulation Parameters
	double dt 	=	1e-4;
	double prod_timestep = 	1e-3;
	double equi_time;
	double prod_time;
	int write_steps 	= 	10;		// Only every write_steps th tiem step is written

	//double rho 		= 	0.1;
	double PBC_boundaries	=	15;
	bool wrap	 	= 	false;	// coordinate wrapping	

	// Solvent Description and Environment
	double zeta 	=	1;
	double k_BT 	=	1;
	double LJ_prefac=	dt/zeta*48*eps;	// Lennard-Jones prefactor
	// Barostat
	bool barostat	= 	true;
	double P0	=	0.1;
	double tau 	= 	0.1;	// Value 0.1 seems to give the best results 

	// Polymer
	double l0 	=	1;		// Rest distance for harmonic bonds
	int chain_length=	50;
	int chain_number=	1;

	// Charge Hopping
	double k_tot 	= 	100;
	double k_intra 	= 	2;		// Intrachain hopping rate
	double k_inter	=	2;		// Interchain hopping rate
	double alpha	=	1;		// Coupling parameter from Marcus theory for interchain hopping rate

	//Brownian Step
	double variance	= 	sqrt(2*k_BT*zeta*dt)/zeta;	// Variance for Brownian motion according to fluctuation dissipation
	double r_rejection_radius = 0.9;
};

#endif
