#ifndef SUPPORT_FUNCTIONS_H
#define SUPPORT_FUNCTIONS_H

//#include "position_handler.h"

//Variables var;

namespace supportFunctions{

	bool positionSearch(std::vector<double> init_dists){
		bool distancesFarApart = true;

		for(int i=0; i<init_dists.size(); i++){
			if( (init_dists[i] < 1-1e-3) && (init_dists[i]>0)){
				distancesFarApart = false;
			}
		}
		return distancesFarApart;
	}

	bool any(std::vector<long double> vec, double distance){

		/* 
			true if a single value in vec meets the condition -> i.e. distance is within excluded volume
		*/

		bool cond = false;

		for(int i=0; i<vec.size(); i++){
			if(vec[i]<distance-1e-3){
				cond=true;	
			}
		}
		return cond;
	}

	std::vector<long double> MonomerDistances(Position_Handler* pos, int polymer_length){
		std::vector<long double> upperDistanceMatrix;

		long double dx,dy,dz,dR;
		for(int i = 1; i<polymer_length; i++){
			for(int j=0; j<i; j++){
				dx = pos[i].r[0]-pos[j].r[0];
				dy = pos[i].r[1]-pos[j].r[1];
				dz = pos[i].r[2]-pos[j].r[2];

				dR = sqrt(pow(dx,2)+pow(dy,2)+pow(dz,2));
				upperDistanceMatrix.push_back(dR);
			}	
		}
		return upperDistanceMatrix;

	}

}

#endif
