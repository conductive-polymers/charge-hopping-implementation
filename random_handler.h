#ifndef RANDOM_HANDLER
#define RANDOM_HANDLER

namespace Random{
	std::mt19937 generator;
	std::normal_distribution<double> dist(0,1);

	void seed(){
		generator.seed(static_cast<unsigned int>(time(NULL)));
	}

	Vector3d randomVector(){
		return Vector3d(dist(generator),dist(generator),dist(generator));
	}
}

#endif
