#ifndef OVITO_H
#define OVITO_H

#include <string>
#include <iomanip>
#include <cstdlib>
#include <fstream>
#include <iostream>

#include "variables.h"
#include "position_handler.h"

class Ovito{
	private:
		std::ofstream BD;
		//Variables var;
		int size;		

		std::ofstream wrapped;
	public:

		Ovito(int iter, int polymer_size){

			size = polymer_size;

			std::string BD_string = "polymer_" + std::to_string(size) + "_" + std::to_string(var.eps) + "_" + std::to_string(var.P0) + "_" + std::to_string(iter) + ".txt";

			std::cout << "Saving data to " << BD_string << std::endl;

			BD.open(BD_string);
		}

		void WriteOvitoStep(Position_Handler* pos, Vector3d charge, int charge_pos, double intrinsic_time){

			//BD << "ITEM: TIMESTEP\n" << intrinsic_time << "\nITEM: NUMBER OF ATOMS\n" << size+1 << "\nITEM: BOX BOUNDS pp pp pp\n0 1\n0 1\n0 1\nITEM: ATOMS id type xs ys zs\n";

			int color;

			for(int i=0; i<=size-1; i++){

				if(i==charge_pos){

					color = 3;

				} else {
					color = 5;
				}

				BD << i << " " << intrinsic_time << " ";
				BD << pos[i].r[0] << " ";
 				BD << pos[i].r[1] << " ";
				BD << pos[i].r[2] << std::endl;

			}
		
			BD << charge_pos << " " << intrinsic_time << " " << charge[0] << " " << charge[1] << " " << charge[2] << std::endl;
		}

		long double wrapper_modulus(double coordinate){
		
			/*
				Modulus function (same as pythonic %)
			*/


			return coordinate - floor(coordinate/var.PBC_boundaries)*var.PBC_boundaries;
		}


		void WriteStep(Vector3d charge, double intrinsic_time){

			BD << intrinsic_time << " ";
			BD << charge[0] << " ";
			BD << charge[1] << " ";
			BD << charge[2] << std::endl;
		}	

		void close(){

			BD.close();
			wrapped.close();
		}
};

#endif
