#ifndef POLYMER_H
#define POLYMER_H

#include <ctime>
#include <random>
#include <vector>

#include "support_functions.h"
#include "charge.h"
#include "random_handler.h"

#define PI 3.1415

typedef std::vector<std::vector<long double>> arr;
typedef std::vector<std::vector<Vector3d>> vec_arr;

class Polymer{
	private:
		Position_Handler* pos;


	public:
		double dt 	= 	var.dt;

		const double zeta 	= var.zeta;
		const double k_BT	= var.k_BT;
		const double C_l	= var.C_l;
		const double eps 	= var.eps;
		const double r_reject	= var.r_rejection_radius;

		double variance 	= sqrt(2*k_BT*zeta*var.dt)/zeta; 
		double LJ_prefac 	= dt/zeta*48*eps;

		int size = var.chain_length*var.chain_number;
		int chain_length = var.chain_length;
		double accepted_steps;
		std::vector<int> rejectedList;


		Charge* charge;	

		double pressure;

		Polymer(){

			accepted_steps = size;

			Random::seed();

			// Defines initial polymer setting 
			pos = new Position_Handler[size];
			PolymerInit(size, pos);
		}

		void PolymerInit(int polymer_length, Position_Handler pos[]){
			const double phi_max  = 2*PI; 
			const double theta_max = PI;
			
			std::random_device rd;
			std::mt19937 gen(rd());

			std::uniform_real_distribution<> f_phi(0,phi_max);
			std::uniform_real_distribution<> f_theta(0,theta_max);

			double theta, phi; 
			std::vector<double> *init_sizes;			

			// Self avoiding walk for monomers			
			for(int j=0; j<polymer_length; j++){
				if(j==0){
					pos[j].reset(polymer_length);
				} else {
					do{
						long double x,y,z;
	
						pos[j].reset(polymer_length);
						init_sizes = new std::vector<double>;
	
						phi = f_phi(gen);
						theta = f_theta(gen);

						if((j-floor(j/chain_length)*chain_length)!=0){
							x = pos[j-1].r[0]+var.l0*sin(theta)*cos(phi);
							y = pos[j-1].r[1]+var.l0*sin(theta)*sin(phi);
							z = pos[j-1].r[2]+var.l0*cos(theta);
						} else {
							x = pos[j-1].r[0]+5*sin(theta)*cos(phi);
							y = pos[j-1].r[1]+5*sin(theta)*sin(phi);
							z = pos[j-1].r[2]+5*cos(theta);

						}
						pos[j].updatePosition(Vector3d(x,y,z));				
					
						for( int m1=1; m1<=j; m1++){
							for( int m2=0; m2<m1; m2++ ){
								init_sizes->push_back(pos[m1].distance_to(pos[m2]));
							}
						}

						pos[j].r_old = pos[j].r;

					} while(!supportFunctions::positionSearch(*init_sizes));
				}
			}

		}


		void initialize_charge(){
			
			// Charge genesis
			charge = new Charge(size,chain_length);
			charge->position = pos[charge->current_monomer].r;

			for(int i=0; i<size;i++){
				pos[i].r_old = pos[i].r;
			}
		}

		void harmonic_step(int monomer, bool barostat_active){

			// Harmonic Potential
			long double dist_abs;
			Vector3d dr, dist_vec; // Total change in positon, distance to monomer+1, distance to monomer-1

			if( ( monomer < size-1 ) && ( (monomer+1-floor((monomer+1)/chain_length)*chain_length)!=0 ) ){
			
				dist_vec = (*pos[monomer].vec_distances)[monomer+1];
				dist_abs = (*pos[monomer].abs_distances)[monomer+1];

				dr = dt/var.zeta*var.C_l * ( (dist_abs-var.l0)*dist_vec/dist_abs  );

				pos[monomer].updatePosition(dr);
				pos[monomer+1].updatePosition(-dr);

				if(barostat_active){
					for(int alpha=0; alpha<3;alpha++){
						pos[monomer+1].chi_mon+=dr[alpha]*dist_vec[alpha]*var.zeta/var.dt;
					}
				}
			}
		}

		void LJ_step(int monomer, bool barostat_active){
			// Lennard Jones potetnial
			Vector3d dr;
			for(int m = monomer; m<size; m++){

				if( ( (*pos[monomer].abs_distances)[m] <= var.rc) && (m!=monomer) && ( ((m!=monomer+1) && (m!=monomer-1)) || monomerAtBeginningAndMonomerAtEnd(monomer,m) || monomerAtEndAndMonomerAtBeginning(monomer,m))  ){ 

					dr = -LJ_prefac* ( pow(var.sigma/(*pos[monomer].abs_distances)[m],14) - 0.5*pow(var.sigma/(*pos[monomer].abs_distances)[m],8) )*(*pos[m].vec_distances)[monomer];

					pos[monomer].updatePosition(dr);
					pos[m].updatePosition(-dr);

					if(barostat_active){
						for(int alpha=0; alpha<3; alpha++){
							pos[m].chi_mon+=(-dr[alpha])*(*pos[m].vec_distances)[monomer][alpha]*var.zeta/var.dt;
						}
					}

				}

			} 
		}
		

		void brownian_step(int monomer, bool barostat_active){
			// Brownian contribution

			Vector3d dr = variance*Random::randomVector();
			pos[monomer].updatePosition(dr);


		}

		bool monomerAtBeginningAndMonomerAtEnd(int m1, int m2){

			if( (m1-floor(m1/chain_length)*chain_length==0) && (m2=m1+1) ){
				return true;
			} else {
				return false;
			}
		}

		bool monomerAtEndAndMonomerAtBeginning(int m1, int m2){
			if( (m1+1-floor((m1+1)/chain_length)*chain_length==0) && (m2=m1-1) ){
				return true;
			} else {
				return false;
			}
			
		}

		void calculate_distances(){
			Vector3d dr;
			for(int monomer_1=0; monomer_1<size; monomer_1++){
				for(int monomer_2=0; monomer_2<size; monomer_2++){
					if(monomer_1 < monomer_2){
						dr = pos[monomer_1].distance_nn(pos[monomer_2]);

						(*pos[monomer_1].vec_distances)[monomer_2] = dr;
						(*pos[monomer_1].abs_distances)[monomer_2] = sqrt(dr[0]*dr[0]+dr[1]*dr[1]+dr[2]*dr[2]);
					} else if(monomer_1 == monomer_2) {
						(*pos[monomer_1].vec_distances)[monomer_2] = Vector3d(0,0,0);
						(*pos[monomer_1].abs_distances)[monomer_2] = 0;
					} else {
						(*pos[monomer_1].vec_distances)[monomer_2] = -(*pos[monomer_2].vec_distances)[monomer_1];
						(*pos[monomer_1].abs_distances)[monomer_2] =  (*pos[monomer_2].abs_distances)[monomer_1];
					}
				}
			}
		}



		void Integration_step(bool barostat_active){
		
			bool accepted;
			long double distance;
			rejectedList = {};
	
			// Polymer position is updated

			for(int monomer=0; monomer<size; monomer++){
				pos[monomer].chi_mon = 0;
				pos[monomer].r_old = pos[monomer].r;
			}

			accepted_steps = 0;
			for(int monomer=0; monomer<size; monomer++){
				LJ_step(monomer, barostat_active);
				harmonic_step(monomer, barostat_active);
				brownian_step(monomer, barostat_active);

				if(!barostat_active){
					accepted = true;
					for(int m = 0; m<size; m++){
						distance = pos[monomer].distance_MC(pos[m],monomer,m);
						if(distance<r_reject && m!=monomer){
							accepted = false;
							rejectedList.push_back(monomer);
							break;
						}
					}
					if(!accepted){
						pos[monomer].r = pos[monomer].r_old;
					} else {
						accepted_steps += 1;
					}
				}
			}

			if(barostat_active){
				accepted_steps = size;
				double chi=0;

				for(int monomer=0; monomer<size; monomer++){
					chi+=pos[monomer].chi_mon;
				}

				berendsen_barostat(chi);
			}
		}

		bool monomerAccepted(int monomer){
			bool accepted = true;
			for(int i=0; i<rejectedList.size(); i++){
				if( monomer==rejectedList[i] ){
					accepted = false;
					break;
				}
			}
			return accepted;
		}

		double barostat_rescaling(double Fr_tot){
			double P = size*var.k_BT/pow(var.PBC_boundaries,3)+Fr_tot/(3*pow(var.PBC_boundaries,3));

			double mu = 1+var.dt/var.tau*(P-var.P0);
			std::cout << P << std::endl;
			pressure = P;
			return mu;
		}

		void berendsen_barostat(double Fr_tot){
			double mu = barostat_rescaling(Fr_tot);

			double scaling = pow(mu,1./3);
			var.PBC_boundaries*=scaling;

			for(int mon=0; mon<size; mon++){
				pos[mon].r*=scaling;
			}

		}

		void charge_dynamics(double random_uniform, double hop_dir, double t_intrinsic){

			// Charge position is udpated
			
			int charge_monomer = charge->current_monomer;
			Vector3d monomer_shift = pos[charge_monomer].r-pos[charge_monomer].r_old;

			charge->Integration_step(random_uniform, hop_dir, pos[charge_monomer].vec_distances, pos[charge_monomer].abs_distances, monomer_shift, t_intrinsic);
		}

		void destroy(){

			// Clears pointers

			delete[] pos;
			delete[] charge;
		}

		Position_Handler* positions(){

			return pos;
		}


		double updateTime(){
			return accepted_steps/size*dt;
		}

};

#endif
