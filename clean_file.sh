#!/bin/bash

name=$1
size1=$2
size2=$3

sed -i "/ITEM/d" $name
sed -i "/2e+1/d" $name
sed -i "1~$2d" $name
sed -i "1~$3d" $name
