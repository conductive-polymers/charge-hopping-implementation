# Charge Hopping Implementation

An simulation of a conductive Polymer with harmonic bonds and non-binding Lennard-Jones potential in an nPT ensemble. Charge hopping is allowed via a Monte Carlo simulation.
