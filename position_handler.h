#ifndef POSITION_HANDLER_H
#define POSITION_HANDLER_H

//#include "support_functions.h"

using namespace Eigen;

class Position_Handler{
	/*
		Framework to keep track of position of all monomers
	*/

	private:
		//Variables var;
		bool wrap_coordinates = var.wrap;

	public:

		// Current position of monomers
		Vector3d r = Vector3d(0,0,0);
		// Old position of monomer to determine hop distance of charge
		Vector3d r_old = Vector3d(0,0,0);

		// Keeps track of distances to other monomers
		std::vector<Vector3d>* vec_distances;
		std::vector<long double>* abs_distances;

		double chi_mon;

		Position_Handler(){

			vec_distances = new std::vector<Vector3d>;
			abs_distances = new std::vector<long double>;
		}

		void populate(int size){

			vec_distances->clear();
			abs_distances->clear();

			vec_distances->resize(size);
			abs_distances->resize(size);
		}

 

		void updatePosition(Vector3d vec){
			r += vec;
			if(wrap_coordinates){
				addPBC();
			}
		}

		Vector3d distance_nn(Position_Handler vec){

			/*
				Calculate distance in PBC as vector
			*/


			Vector3d vecdist = vecPBC(vec.r-r);

			return vecdist;
		}

		long double distance_MC(Position_Handler vec, int m1, int m2){
			Vector3d dr_vec;
			if(m1<m2){
				dr_vec = vecPBC(r-vec.r_old);
				return sqrt(dr_vec[0]*dr_vec[0]+dr_vec[1]*dr_vec[1]+dr_vec[2]*dr_vec[2]);
			} else if (m1==m2) {
				return 0;
			} else {
				dr_vec = vecPBC(r-vec.r);
				return sqrt(dr_vec[0]*dr_vec[0]+dr_vec[1]*dr_vec[1]+dr_vec[2]*dr_vec[2]);
			}		
		}

		long double distance_to(Position_Handler vec){

			/*
				Calculates absolute value of distance in PBC
			*/

			Vector3d rp = distance_nn(vec);

			return sqrt(pow(rp[0],2)+pow(rp[1],2)+pow(rp[2],2));
		}

		long double wrapper_modulus(double coordinate){
		
			/*
				Modulus function (same as pythonic %)
			*/


			return coordinate - floor(coordinate/var.PBC_boundaries)*var.PBC_boundaries;
		}

		void reset(int size){
			r = Vector3d(0,0,0);

			populate(size);
		}
	
		void addPBC(){

			for(int i=0; i<3;i++){

				if(r[i]<0 || r[i]>10){

					r[i] = wrapper_modulus(r[i]);	
				}
			}
		}

		Vector3d vecPBC(Vector3d distance){
			
			Vector3d wrapped_distance;

			for(int i=0;i<3;i++){
				double dist = fmod(distance[i],var.PBC_boundaries);
				if(dist > var.PBC_boundaries/2){
					wrapped_distance[i] = std::abs(dist)-var.PBC_boundaries;
				} else if (dist< -var.PBC_boundaries/2){
					wrapped_distance[i] = var.PBC_boundaries-std::abs(dist);
				} else {
					wrapped_distance[i] = dist;
				}
			}

			return wrapped_distance;
		}
};

#endif

