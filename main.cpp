#include <iostream>
#include <vector>
#include <cmath>
#include <ctime>
#include <stdio.h>
#include <math.h>
#include <random>
#include <cstdlib>
#include <Eigen/Dense>

#include "variable_handler.h"
#include "variables.h"
#include "position_handler.h"
#include "polymer.h"
#include "simulation.h"
#include "ovito.h"
#include "charge.h"

typedef std::vector<long double> position;

int main(int argc, char *argv[]){
	
	if(argc == 2){
		int iteration 	= std::atoi(argv[1]);


		int len_polymer = var.chain_length;

		// Redefine equilibration and production time to Rouse time of given polymer length
		var.equi_time = 1*pow(len_polymer/3.141,2)/6;
		var.prod_time = 5*pow(len_polymer/3.141,2)/6;

		// Start Simulation
		Simulation sim = Simulation(iteration);

	} else {

		std::cout << "Invalid input." << std::endl;

	}
}



