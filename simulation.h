#ifndef SIMULATION_H
#define SIMULATION_H

#include <chrono>
#include <random>
#include <iostream>
#include <ctime>

#include "ovito.h"
#include "charge.h"

class Simulation{
	private:
		//Variables var;
	public: 
		int prod_time = var.prod_time;

		const int equi_time	= var.equi_time;
		
		// Time Step
		double dt;

		// Intrinsic time steps (only differs from simulated time when steps get rejected)
		double t_intrinsic 	= 0;

		// True if pressure needs to be controlled during equilibration
		bool barostat		= var.barostat;
		int N			= var.chain_length*var.chain_number;
		int write_step = var.write_steps;

		Simulation(int iter){
		
			// Defines Polymer
			Polymer p = Polymer();


			// Begins Simulation with polymer and iteration number (keeps track of simulation ID)
			RunSimulation(p,iter);
		}

		void RunSimulation(Polymer polymer, int iter){

			time_t simstart = time(0);
			std::cout << "Beginning Simulationin\n Start: " << ctime(&simstart) << std::endl;

			// The data is stored in a file through the Ovito class
			Ovito ovito = Ovito(iter, polymer.size);

			// Define random number generator for Simulation class
			std::random_device rd;
			std::mt19937 generator;
			generator.seed(static_cast<unsigned int >(time(NULL)));

			std::normal_distribution<double> dist(0,1);
			std::uniform_real_distribution<double> uniform(0,1);
			

			// Begin Equilibration without charge; No data is written here yet
			// Barostat Equilibration						
			if (barostat){
				int count = 0;
				polymer.dt = 1e-4;
				while(t_intrinsic < 5){
					polymer.calculate_distances();
					if(count%10==0){
						polymer.Integration_step(true);
					} else {
						polymer.Integration_step(false);
					}
					t_intrinsic += polymer.updateTime();
					count += 1;
				}

				t_intrinsic = 0;
			}

		 	// Polymer Equilibration
		 	polymer.dt = var.prod_timestep;
			polymer.variance = sqrt(2*var.k_BT*var.zeta*polymer.dt)/var.zeta;
			polymer.LJ_prefac = polymer.dt/var.zeta*48*var.eps;
			while(t_intrinsic < equi_time){
				polymer.calculate_distances();
				polymer.Integration_step(false);
				t_intrinsic += polymer.updateTime();
				progress(t_intrinsic,equi_time);
			}

			t_intrinsic = 0;
			// Production Run with Charge initialized to a monomer
			polymer.initialize_charge();

			int count = 0;
			while(t_intrinsic < prod_time){
			
				polymer.calculate_distances();

				// Charge either hops or stays on monomer, the variables are the random numbers for the kMC framework
				polymer.charge_dynamics(uniform(generator), uniform(generator), t_intrinsic);

				t_intrinsic += polymer.updateTime();

				// Writes only every write_step th conformation with the ovito class
				if(count%write_step==0){
					if(var.writeOvito){
						// Ovito style output of entire polymer and charge
						ovito.WriteOvitoStep(polymer.positions(), polymer.charge->position, polymer.charge->current_monomer, t_intrinsic);

					} else {
						// Only cartesian coordinates of charge are saved
						ovito.WriteStep(polymer.charge->position, t_intrinsic);

					}
				}

				// Polymer motion is updated
				polymer.Integration_step(false);
				count+=1;
			}
			
			time_t simend = time(0);
			std::cout << "Simulation Finished\n Simulation ended at: " << ctime(&simend) << std::endl;
			std::cout << "------------------------- \n Summary: " << std::endl;
			std::cout << "Total particles simulated: " << N << std::endl;
			std::cout << equi_time + prod_time << " time steps simulated in " << (simend-simstart)/3600 << "h" << std::endl;
			std::cout << "Total Bound Hops: " << polymer.charge->n_bound << "\nTotal Unbound Hops: " << polymer.charge->n_unbound << std::endl;

			std::cout << "PBC Length: " << var.PBC_boundaries << std::endl;

			// Clears all pointers 
			polymer.destroy();
			ovito.close();

			std::cout << "Done!" << std::endl;

			std::string Summary = "polymer_" + std::to_string(N) + "_" + std::to_string(var.eps) + "_" + std::to_string(var.P0) + "_" + std::to_string(iter);
			std::ofstream summary;
			summary.open(Summary);
			summary << "PBC Length: " << var.PBC_boundaries << std::endl;
			summary.close();

		}

		void progress(int step, int total_steps){

			/*
				Shows simulation progress
			*/

			double perc = (double)step/(double)total_steps*100;
			std::cout << perc << "%\r";
		}

};

#endif
